﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SheepCommand : MonoBehaviour
{
    public float holdRange = 0.1f;
    public float summonRange = 10f;
    public float raiseAmount = 1f;
    public float raiseDuration = 1f;
    public float comeDuration = 0.5f;

    SheepMovement sheep;
    Rigidbody2D rbSheep;
    Collider2D[] colsSheep;
    Animator animSheep;
    PlayerMovement commander;
    CharacterController2D ccCommander;
    Animator animCommander;
    [SerializeField] bool held;
    [SerializeField] bool swolen;
    [SerializeField] Transform groundCheck;
    Transform holdingPosition;
    ColdControl coldControl;

    void Start()
    {
        // Find the sheep to command and tell the sheep to follow me.
        sheep = FindObjectOfType<SheepMovement>();
        sheep.targetToFollow = transform;
        rbSheep = sheep.GetComponent<Rigidbody2D>();
        //ccSheep = sheep.GetComponent<CharacterController2D>();
        colsSheep = sheep.GetComponents<Collider2D>();
        animSheep = sheep.GetComponent<Animator>();
        commander = GetComponent<PlayerMovement>();
        animCommander = commander.GetComponentInChildren<Animator>();
        ccCommander = commander.GetComponent<CharacterController2D>();
        holdingPosition = transform.Find("Holding Position");
        coldControl = FindObjectOfType<ColdControl>();
    }

    [SerializeField] bool m_inputEnabled = true;
    public bool InputEnabled { get { return m_inputEnabled; } set { m_inputEnabled = value; } }

    void Update()
    {
        if (InputEnabled)
        {
            // Summon
            if (Input.GetButtonDown("Use"))
            {
                // If within the holding range, hold.
                if (WithinRange(holdRange))
                {
                    if (!held)
                        Hold();
                    else
                        Release();
                }

                // If within the summon range, summon.
                else if (WithinHorizontalRange(summonRange))
                {
                    sheep.MoveTowardTarget();
                }

            }

            // Swell
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (!swolen)
                {
                    sheep.Swell();
                    swolen = true;

                }
                else
                {
                    sheep.Shrink();
                    swolen = false;

                }
            }
        }
    }

    /// <summary>
    /// Hold a sheep. The sheep is turned child of this object, and raised up. The holder's speed is slowed down.
    /// </summary>
    public void Hold()
    {
        if (!held)
        {
            // The sheep will come to the player, and then the player holds her up
            StartCoroutine(C_HoldSheepAnim());

            //// Set the sheep state.
            //sheep.FollowTarget = false;
            //rbSheep.isKinematic = true;
            //foreach (var col in colsSheep)
            //    col.enabled = false;

            //// Set moving together with the commander.
            //sheep.transform.parent = transform;

            //// Move the sheep toward the player, then up.
            //Sequence sequence = DOTween.Sequence();
            //sequence.PrependInterval(0.1f);
            //sequence.Append(rbSheep.DOMoveX(commander.transform.position.x, comeDuration));
            //sequence.Append(rbSheep.DOMoveY(sheep.transform.position.y + raiseAmount, raiseDuration));
            //sequence.Play();
            //sequence.OnComplete(HoldAnimOnComplete);
        }
    }

    //void HoldAnimOnComplete()
    //{
    //    // Set the commander capability.
    //    commander.Speed = commander.slowSpeed;
    //    commander.Jumpable = false;

    //    // Set the sheep animator.
    //    animSheep.SetBool("Held", true);

    //    rbSheep.velocity = Vector3.zero;

    //    held = true;
    //}

    float currentLerpTime;
    float sheepGroundY;
    IEnumerator C_HoldSheepAnim()
    {
        // During the animation, input is disabled.
        InputEnabled = false;

        // Display the sheep at the top.
        SpriteRenderer[] renderers = sheep.GetComponentsInChildren<SpriteRenderer>();
        foreach (var renderer in renderers)
        {
            renderer.sortingLayerName = "Top";
        }

        // Girl faces the sheep and the sheep faces the girl.
        var girlPos = commander.transform.position;
        var sheepPos = sheep.transform.position;
        sheepGroundY = sheepPos.y;
        if (girlPos.x > sheepPos.x)
        {
            // Sheep left girl right.
            // Sheep faces right girl faces left.
            sheep.FacingRight = true;
            ccCommander.FacingRight = false;
        }
        else
        {
            sheep.FacingRight = false;
            ccCommander.FacingRight = true;
        }

        // Turn off sheep collider
        rbSheep.isKinematic = true;
        foreach (var col in colsSheep)
            col.enabled = false;

        // Sheep runs to the girl.
        currentLerpTime = 0f;
        float duration = 1f;
        var sheepTargetPos = girlPos; sheepTargetPos.y = sheepPos.y; sheepTargetPos.z = sheepPos.z;
        while (true)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > duration)
            {
                //currentLerpTime = duration;

                break;
            }
            float perc = currentLerpTime / duration;
            sheep.transform.position = Vector3.Lerp(sheepPos, sheepTargetPos, perc);
            yield return new WaitForFixedUpdate();
        }
        sheepPos = sheep.transform.position;

        // By this time the sheeps is at the girl's place.
        // Girl hold the sheep.
        animCommander.SetBool("Held", true);
        // Sheep goes up to the holding position.
        currentLerpTime = 0f;
        duration = 1f;
        sheepTargetPos.y = holdingPosition.position.y;
        while (true)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > duration)
            {
                break;
            }
            float perc = currentLerpTime / duration;
            sheep.transform.position = Vector3.Lerp(sheepPos, sheepTargetPos, perc);
            yield return new WaitForFixedUpdate();
        }

        // By this time the sheeps is in the girl's arms.

        // Sheep is feeling cozy being held.
        animSheep.SetBool("Held", true);


        // Animation is over. Input reenabled.
        InputEnabled = true;
        // Set held boolean as true.
        held = true;
        // Bind the sheep and the girl.
        sheep.BeingHeld = true;
        sheep.targetToFollow = holdingPosition;

        // Slow down the girl.
        commander.Speed = commander.slowSpeed;
        commander.Jumpable = false;

        // Feel warm.
        if (coldControl)
            coldControl.isWarm = true;
    }

    public void Release()
    {
        if (held)
        {
            // The sheep will go down and run away.
            StartCoroutine(C_ReleaseSheepAnim());

            //// Put the sheep down.
            //Sequence sequence = DOTween.Sequence();
            //sequence.Append(rbSheep.DOMoveY(sheep.transform.position.y - raiseAmount, raiseDuration));
            //sequence.InsertCallback(raiseDuration, () =>
            //{
            //    // Force the sheep facing the same way as the commander.
            //    sheep.FacingRight = ccCommander.FacingRight;
            //});
            //if (sheep.FacingRight)
            //    sequence.Append(rbSheep.DOMoveX(commander.transform.position.x + Mathf.Abs(holdRange), comeDuration));
            //else
            //    sequence.Append(rbSheep.DOMoveX(commander.transform.position.x - Mathf.Abs(holdRange), comeDuration));
            //sequence.Play();

            //sequence.OnComplete(ReleaseAnimOnComplete);

            //sheep.FacingRight = ccCommander.FacingRight;
            //var hr = sheep.FacingRight ? holdRange : -holdRange;
            //Vector3 newPos = sheep.transform.position;
            //newPos.y = sheep.transform.position.y - raiseAmount;
            //newPos.x = sheep.transform.position.x + hr;
            //sheep.transform.position = newPos;
            //ReleaseAnimOnComplete();
        }
    }

    void ReleaseAnimOnComplete()
    {
        // Unbind the sheep and the commander.
        sheep.transform.parent = null;

        // Set the commander capability.
        commander.Speed = commander.normalSpeed;
        commander.Jumpable = true;

        // Set the sheep animator.
        animSheep.SetBool("Held", false);

        // Set the sheep state.
        sheep.FollowTarget = false;
        rbSheep.isKinematic = false;
        foreach (var col in colsSheep)
            col.enabled = true;

        rbSheep.velocity = Vector3.zero;

        held = false;

    }

    IEnumerator C_ReleaseSheepAnim()
    {
        // During the animation, input is disabled.
        InputEnabled = false;

        // The sheep faces the same direction the girl faces.
        sheep.OverrideFacing = true;
        sheep.FacingRight = ccCommander.FacingRight;

        // The sheep nolonger in cozy.
        animSheep.SetBool("Held", false);
        // Girl nolonger holds the sheep.
        animCommander.SetBool("Held", false);

        // The sheep decends to the ground
        var sheepPos = sheep.transform.position;
        currentLerpTime = 0f;
        float duration = 1f;
        var sheepTargetPos = sheepPos; sheepTargetPos.y = sheepGroundY;
        while (true)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > duration)
            {
                //currentLerpTime = duration;

                break;
            }
            float perc = currentLerpTime / duration;
            sheep.transform.position = Vector3.Lerp(sheepPos, sheepTargetPos, perc);
            yield return new WaitForFixedUpdate();
        }
        sheepPos = sheep.transform.position;

        // By this time the sheep is on the ground.
        sheep.OverrideFacing = false;
        sheep.BeingHeld = false;

        // Sheep runs away
        // Run.
        float moveAmount = sheep.speed;
        float facing = (sheep.FacingRight ? 1 : -1);
        while (sheep.CollideWithTheGirl())
        {
            sheep.transform.position += facing * Vector3.right * moveAmount * Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        // Enable the collider.
        foreach (var col in colsSheep)
            col.enabled = true;
        // Enable the rigidbody.
        rbSheep.isKinematic = false;

        // Resume the girl's speed.
        commander.Speed = commander.normalSpeed;
        commander.Jumpable = true;
        // Display the sheep at its own level.
        SpriteRenderer[] renderers = sheep.GetComponentsInChildren<SpriteRenderer>();
        foreach (var renderer in renderers)
        {
            renderer.sortingLayerName = "Companion";
        }

        // Feel cold again.
        if (coldControl)
            coldControl.isWarm = false;

        // Animation is over. Input reenabled.
        InputEnabled = true;
        // Set held boolean as false.
        held = false;
    }

    bool WithinRange(float range)
    {
        var sqrDis = (transform.position - sheep.transform.position).sqrMagnitude;
        if (sqrDis <= range * range)
            return true;
        return false;
    }

    bool WithinHorizontalRange(float range)
    {
        var dis = transform.position.x - sheep.transform.position.x;
        dis = Mathf.Abs(dis);
        if (dis <= range)
            return true;
        return false;
    }

    private void OnDrawGizmos()
    {
        if (!groundCheck) return;

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(groundCheck.position + Vector3.left * holdRange, groundCheck.position + Vector3.right * holdRange);
    }
}
