﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepMovement : MonoBehaviour
{
    public float speed = 40;
    public Transform targetToFollow;
    public float stopDistance = 0.1f;
    public float revertGravity = -0.1f;
    public float normalGravity = 1f;
    public LayerMask collideLayer;

    //CharacterController2D cc;
    Rigidbody2D rb;
    Animator anim;
    [SerializeField] bool followTarget = false;

    public bool FollowTarget
    {
        set { followTarget = value; if (!value) StopMoving(); }
        get { return followTarget; }
    }

    public bool BeingHeld = false;

    private void Start()
    {
        //cc = GetComponent<CharacterController2D>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        rb.gravityScale = normalGravity;
    }

    private void Update()
    {
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));

        if (BeingHeld)
        {
            // Override the position as the target's position.
            rb.position = targetToFollow.position;
        }
    }

    public void MoveTowardTarget()
    {
        StopAllCoroutines();
        StartCoroutine(C_MoveTowardsTarget());
    }

    //public void MoveTo(Vector3 position)
    //{
    //    StopAllCoroutines();
    //    StartCoroutine(C_MoveTo(position));
    //}

    public void StopMoving()
    {
        StopAllCoroutines();
    }


    public bool OverrideFacing = false;
    IEnumerator C_MoveTowardsTarget()
    {
        followTarget = true;

        var targetedPosition = targetToFollow.position;
        var dir = Vector3.right * (targetedPosition.x - transform.position.x);

        // Facing
        if (!OverrideFacing)
        {
            if (dir.x > 0) FacingRight = true;
            else FacingRight = false;
        }

        // If collides with player or within a stop distance, stops.
        var stuff = Physics2D.OverlapCircle(transform.position, stopDistance, collideLayer);
        var withinStopDistance = WithinHorizontalRange(stopDistance, targetToFollow.transform.position);

        while (!withinStopDistance && stuff == null)
        {
            var s = dir.normalized.x * speed;
            //rb.MovePosition(rb.position + Vector2.right * s * Time.deltaTime);
            rb.velocity = rb.position + Vector2.right * s;
            yield return new WaitForFixedUpdate();

            stuff = Physics2D.OverlapCircle(transform.position, stopDistance, collideLayer);
            withinStopDistance = WithinHorizontalRange(stopDistance, targetToFollow.transform.position);
        }

        followTarget = false;
    }

    //IEnumerator C_MoveTo(Vector3 position)
    //{
    //    // Moves to the x of this position.
    //    var dir = Vector3.right * (position.x - transform.position.x);
    //    // Facing
    //    if (!OverrideFacing)
    //    {
    //        if (dir.x > 0) FacingRight = true;
    //        else FacingRight = false;
    //    }

    //    // If collides with player or within a stop distance, stops.
    //    var stuff = Physics2D.OverlapCircle(transform.position, stopDistance, collideLayer);
    //    var withinStopDistance = WithinHorizontalRange(stopDistance, targetToFollow.transform.position);

    //    while (!withinStopDistance && stuff == null)
    //    {
    //        var s = dir.normalized.x * speed;
    //        rb.MovePosition(rb.position + Vector2.right * s * Time.deltaTime);
    //        yield return new WaitForFixedUpdate();

    //        stuff = Physics2D.OverlapCircle(transform.position, stopDistance, collideLayer);
    //        withinStopDistance = WithinHorizontalRange(stopDistance, targetToFollow.transform.position);
    //    }
    //}

    public bool CollideWithTheGirl()
    {
        var stuff = Physics2D.OverlapCircle(transform.position, stopDistance, collideLayer);
        return stuff != null;
    }

    public void Swell()
    {
        anim.SetTrigger("Swell");
        rb.gravityScale = revertGravity;
    }

    public void Shrink()
    {
        anim.SetTrigger("Shrink");
        rb.gravityScale = normalGravity;
    }

    public void SwellEnd()
    {
        anim.SetBool("Swolen", true);
    }

    public void ShrinkEnd()
    {
        anim.SetBool("Swolen", false);
    }

    public bool FacingRight
    {
        get { return transform.lossyScale.x < 0; }
        set
        {
            if (value)
            {
                Vector3 theScale = transform.lossyScale;
                theScale.x = -Mathf.Abs(theScale.x);
                transform.localScale = theScale;
            }
            else
            {
                Vector3 theScale = transform.lossyScale;
                theScale.x = Mathf.Abs(theScale.x);
                transform.localScale = theScale;
            }
        }
    }

    bool WithinHorizontalRange(float range, Vector3 position)
    {
        var dis = transform.position.x - position.x;
        dis = Mathf.Abs(dis);
        if (dis <= range)
            return true;
        return false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, stopDistance);
        //Gizmos.DrawLine(transform.position, sheep.transform.position);
        //var dist = (transform.position - sheep.transform.position).magnitude;
        //print(dist);
    }
}
