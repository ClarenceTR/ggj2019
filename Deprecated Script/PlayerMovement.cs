﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    CharacterController2D cc;
    public float normalSpeed = 80;
    public float slowSpeed = 40;

    [SerializeField] float speed;
    bool jump;
    [SerializeField] bool jumpable = true;
    float h;
    Animator anim;
    Rigidbody2D rb;

    public float Speed { set { speed = value; } }
    public bool Jumpable { get { return jumpable; } set { jumpable = value; } }

    void Start()
    {
        cc = GetComponent<CharacterController2D>();
        rb = GetComponent<Rigidbody2D>();
        speed = normalSpeed;
        anim = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        h = Input.GetAxis("Horizontal");

        if (jumpable && Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

    void FixedUpdate()
    {
        cc.Move(speed * h * Time.deltaTime, false, jump);
        jump = false;

        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
    }

    public void PlayIdleAnim()
    {
        anim.SetTrigger("Reset");
    }
}
