// GENERATED AUTOMATICALLY FROM 'Assets/This Project/InputMaster.inputactions'

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.Input;


[Serializable]
public class InputMaster : InputActionAssetReference
{
    public InputMaster()
    {
    }
    public InputMaster(InputActionAsset asset)
        : base(asset)
    {
    }
    private bool m_Initialized;
    private void Initialize()
    {
        // Player
        m_Player = asset.GetActionMap("Player");
        m_Player_Jump = m_Player.GetAction("Jump");
        if (m_PlayerJumpActionStarted != null)
            m_Player_Jump.started += m_PlayerJumpActionStarted.Invoke;
        if (m_PlayerJumpActionPerformed != null)
            m_Player_Jump.performed += m_PlayerJumpActionPerformed.Invoke;
        if (m_PlayerJumpActionCancelled != null)
            m_Player_Jump.cancelled += m_PlayerJumpActionCancelled.Invoke;
        m_Player_Move = m_Player.GetAction("Move");
        if (m_PlayerMoveActionStarted != null)
            m_Player_Move.started += m_PlayerMoveActionStarted.Invoke;
        if (m_PlayerMoveActionPerformed != null)
            m_Player_Move.performed += m_PlayerMoveActionPerformed.Invoke;
        if (m_PlayerMoveActionCancelled != null)
            m_Player_Move.cancelled += m_PlayerMoveActionCancelled.Invoke;
        m_Initialized = true;
    }
    private void Uninitialize()
    {
        if (m_PlayerActionsCallbackInterface != null)
        {
            Player.SetCallbacks(null);
        }
        m_Player = null;
        m_Player_Jump = null;
        if (m_PlayerJumpActionStarted != null)
            m_Player_Jump.started -= m_PlayerJumpActionStarted.Invoke;
        if (m_PlayerJumpActionPerformed != null)
            m_Player_Jump.performed -= m_PlayerJumpActionPerformed.Invoke;
        if (m_PlayerJumpActionCancelled != null)
            m_Player_Jump.cancelled -= m_PlayerJumpActionCancelled.Invoke;
        m_Player_Move = null;
        if (m_PlayerMoveActionStarted != null)
            m_Player_Move.started -= m_PlayerMoveActionStarted.Invoke;
        if (m_PlayerMoveActionPerformed != null)
            m_Player_Move.performed -= m_PlayerMoveActionPerformed.Invoke;
        if (m_PlayerMoveActionCancelled != null)
            m_Player_Move.cancelled -= m_PlayerMoveActionCancelled.Invoke;
        m_Initialized = false;
    }
    public void SetAsset(InputActionAsset newAsset)
    {
        if (newAsset == asset) return;
        var PlayerCallbacks = m_PlayerActionsCallbackInterface;
        if (m_Initialized) Uninitialize();
        asset = newAsset;
        Player.SetCallbacks(PlayerCallbacks);
    }
    public override void MakePrivateCopyOfActions()
    {
        SetAsset(ScriptableObject.Instantiate(asset));
    }
    // Player
    private InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private InputAction m_Player_Jump;
    [SerializeField] private ActionEvent m_PlayerJumpActionStarted;
    [SerializeField] private ActionEvent m_PlayerJumpActionPerformed;
    [SerializeField] private ActionEvent m_PlayerJumpActionCancelled;
    private InputAction m_Player_Move;
    [SerializeField] private ActionEvent m_PlayerMoveActionStarted;
    [SerializeField] private ActionEvent m_PlayerMoveActionPerformed;
    [SerializeField] private ActionEvent m_PlayerMoveActionCancelled;
    public struct PlayerActions
    {
        private InputMaster m_Wrapper;
        public PlayerActions(InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Jump { get { return m_Wrapper.m_Player_Jump; } }
        public ActionEvent JumpStarted { get { return m_Wrapper.m_PlayerJumpActionStarted; } }
        public ActionEvent JumpPerformed { get { return m_Wrapper.m_PlayerJumpActionPerformed; } }
        public ActionEvent JumpCancelled { get { return m_Wrapper.m_PlayerJumpActionCancelled; } }
        public InputAction @Move { get { return m_Wrapper.m_Player_Move; } }
        public ActionEvent MoveStarted { get { return m_Wrapper.m_PlayerMoveActionStarted; } }
        public ActionEvent MovePerformed { get { return m_Wrapper.m_PlayerMoveActionPerformed; } }
        public ActionEvent MoveCancelled { get { return m_Wrapper.m_PlayerMoveActionCancelled; } }
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled { get { return Get().enabled; } }
        public InputActionMap Clone() { return Get().Clone(); }
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                Jump.cancelled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                Move.cancelled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                Jump.started += instance.OnJump;
                Jump.performed += instance.OnJump;
                Jump.cancelled += instance.OnJump;
                Move.started += instance.OnMove;
                Move.performed += instance.OnMove;
                Move.cancelled += instance.OnMove;
            }
        }
    }
    public PlayerActions @Player
    {
        get
        {
            if (!m_Initialized) Initialize();
            return new PlayerActions(this);
        }
    }
    private int m_KeyboardandMouseSchemeIndex = -1;
    public InputControlScheme KeyboardandMouseScheme
    {
        get

        {
            if (m_KeyboardandMouseSchemeIndex == -1) m_KeyboardandMouseSchemeIndex = asset.GetControlSchemeIndex("Keyboard and Mouse");
            return asset.controlSchemes[m_KeyboardandMouseSchemeIndex];
        }
    }
    [Serializable]
    public class ActionEvent : UnityEvent<InputAction.CallbackContext>
    {
    }
}
public interface IPlayerActions
{
    void OnJump(InputAction.CallbackContext context);
    void OnMove(InputAction.CallbackContext context);
}
