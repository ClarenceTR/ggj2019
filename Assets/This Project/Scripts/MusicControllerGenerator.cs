﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MusicControllerGenerator : MonoBehaviour
{
    [SerializeField] GameObject pf_musicController;

    private void Start()
    {
        Instantiate(pf_musicController);
        DontDestroyOnLoad(gameObject);
    }
}