﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class ColdBar : MonoBehaviour
{
    [SerializeField] Image coldBar;
    [SerializeField] GameObject coldIndicator;

    public float maxValue = 110f;
    public float coldThreshold = 60f;
    public float dyingThreshold = 100f;
    [Range(0, 10)]
    public float decreaseSpeed = 10f;
    [Range(0, 10)]
    public float increaseSpeed = 3f;

    bool m_active = true;
    [SerializeField] float m_value;
    bool m_feelsCold = false;

    private void Start()
    {
        m_value = 0;
        Display();
    }

    private void Update()
    {
        if (!m_active) return;
        Evaluate();
        Display();
    }

    public bool Active { get => m_active; set => m_active = value; }
    public float Coldness
    {
        get => m_value;
        set
        {
            m_value = Mathf.Clamp(value, 0, maxValue);
        }
    }
    public float DyingDegree { get => (m_value - dyingThreshold) / (maxValue - dyingThreshold); }   // If negative, not dying.
    public bool FeelsCold { get => m_feelsCold; }
    public bool IsDying { get { return Coldness > dyingThreshold; } }
    public bool IsDead { get { return Mathf.Abs(Coldness - maxValue) < 0.001f; } }
    public bool IsWarm { get => IsWarmedBySheep | IsWarmedByTorch; }
    public bool IsWarmedByTorch { get; set; }
    public bool IsWarmedBySheep { get; set; }

    void Evaluate()
    {
        if (IsWarm)
        {
            Coldness -= decreaseSpeed * Time.deltaTime;
        }
        else
        {
            Coldness += increaseSpeed * Time.deltaTime;
        }

        // Store if feels cold.
        m_feelsCold = Coldness > coldThreshold;
    }

    void Display()
    {
        float ratio = m_value / dyingThreshold;
        coldBar.fillAmount = ratio;

        if (m_feelsCold)
            // Display the coldness indicator.
            coldIndicator.SetActive(true);
        else
            // Hide the coldness indicator.
            coldIndicator.SetActive(false);
    }

}
