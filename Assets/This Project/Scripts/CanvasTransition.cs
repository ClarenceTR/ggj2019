﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasTransition : MonoBehaviour
{
    [SerializeField] CanvasGroup mask;

    public void FadeInScene(float duration)
    {
        StartCoroutine(C_FadeAlpha(0, duration));
    }

    public void FadeOutScene(float duration)
    {
        StartCoroutine(C_FadeAlpha(1, duration));
    }

    float currentLerpTime;
    IEnumerator C_FadeAlpha(float to, float duration)
    {
        float from = mask.alpha;
        currentLerpTime = 0f;
        while (true)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > duration)
            {
                //currentLerpTime = duration;

                break;
            }
            float perc = currentLerpTime / duration;
            mask.alpha = Mathf.Lerp(from, to, perc);
            yield return new WaitForFixedUpdate();
        }

        if (to < 0.1f)
        {
            mask.blocksRaycasts = false;
            mask.interactable = false;
        }
        else if (to>0.9f)
        {
            mask.blocksRaycasts = true;
            mask.interactable = true;
        }
    }
}
