﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Luminosity.IO;
using UnityEngine.Events;
using System;

public class SceneTransitionController : Singleton<SceneTransitionController>
{
    //public float fadeDuration = 0.5f;
    //public float fadeDelay = 0f;
    //public string nextScene = "SampleScene";

    //[SerializeField] CanvasGroup m_maskGroup;
    //bool m_sceneTransitionOver;
    //public bool IsSceneTransitionOver { get => m_sceneTransitionOver; }

    Dictionary<Scene, SceneTransitor> loadedScenes;
    Scene TopScene => SceneManager.GetSceneAt(Mathf.Clamp(SceneManager.sceneCount - 1, 0, int.MaxValue));
    public bool MultipleScenes => SceneManager.sceneCount > 1;

    private void Start()
    {
        loadedScenes = new Dictionary<Scene, SceneTransitor>();
        loadedScenes.Add(SceneManager.GetActiveScene(), FindObjectOfType<SceneTransitor>());

        // Bind the OnSceneLoaded event for loading scenes on top.
        SceneManager.sceneLoaded += delegate (Scene scene, LoadSceneMode loadSceneMode)
        {
            if (loadSceneMode == LoadSceneMode.Additive)
            {
                var goes = scene.GetRootGameObjects();
                bool f_st = false, f_stc = false, f_im = false;
                foreach (var go in goes)
                {
                    // Register the Scene Transitor in this added scene.
                    if (!f_st)
                    {
                        var st = go.GetComponentInChildren<SceneTransitor>();
                        if (st) { loadedScenes.Add(scene, st); f_st = true; }
                    }

                    // Destroy the Scene Transitor Controller if there's any.
                    if (!f_stc)
                    {
                        var stc = go.GetComponentInChildren<SceneTransitionController>();
                        if (stc) { Destroy(stc); f_stc = true; }
                    }

                    // Disable input manager if there's any.
                    if (!f_im)
                    {
                        var im = go.GetComponentInChildren<InputManager>();
                        if (im) { im.enabled = false; f_im = true; }
                    }

                    if (f_st && f_stc && f_im) return;
                }
                if (!f_st)
                    loadedScenes.Add(scene, null);
            }

            // After these things are done, unload self.
        };

        //m_sceneTransitionOver = false;
        //if (m_maskGroup != null)
        //{
        //    m_maskGroup.alpha = 1;
        //    m_maskGroup.interactable = true;
        //    m_maskGroup.blocksRaycasts = true;
        //}

        //StartCoroutine(C_FadeInTheScene());
    }

    //IEnumerator C_FadeInTheScene()
    //{
    //    yield return new WaitForSeconds(fadeDelay);

    //    // Fade in the scene.
    //    m_fadingAnimationPlaying = true;
    //    m_maskGroup.DOFade(0, fadeDuration).OnStart(delegate () { }).OnComplete(delegate ()
    //    {
    //        m_fadingAnimationPlaying = false;
    //        m_sceneTransitionOver = true;
    //        m_maskGroup.interactable = false;
    //        m_maskGroup.blocksRaycasts = false;
    //    });
    //}

    //bool m_fadingAnimationPlaying = false;

    public void GoToScene(string sceneName)
    {
        // Get current scene
        var currScene = CurrentScene;
        var currSt = loadedScenes[currScene];
        currSt.FadeOutTheScene(delegate ()
        {
            SceneManager.LoadSceneAsync(sceneName);
        });
    }

    public void GoToNextScene()
    {
        // Get the current scene to know what next scene is.
        var currScene = CurrentScene;
        var currSt = loadedScenes[currScene];
        currSt.FadeOutTheScene(delegate ()
        {
            SceneManager.LoadSceneAsync(currSt.nextScene);
        });
    }

    /// <summary>
    /// Instead of going to another scene, it loads the game over scene on top of the current scene.
    /// </summary>
    public void LoadSceneOnTop(string sceneName, bool fadeOut = true)
    {
        var currScene = CurrentScene;
        var currSt = loadedScenes[currScene];
        Action OnFadingComplete = delegate ()
        {
            SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            OnLoadTopScene.Invoke();
        };
        if (fadeOut)
            currSt.FadeOutTheScene(OnFadingComplete);
        else
            OnFadingComplete();
    }

    public void UnloadTopScene()
    {
        if (MultipleScenes)
        {
            Scene topScene = TopScene;
            var topSt = loadedScenes[topScene];
            topSt.FadeOutTheScene(delegate ()
            {
                loadedScenes.Remove(topScene);
                SceneManager.UnloadSceneAsync(topScene);
                OnUnloadTopScene.Invoke();
            });
        }
    }

    [HideInInspector] public UnityEvent OnLoadTopScene;
    [HideInInspector] public UnityEvent OnUnloadTopScene;

    public Scene CurrentScene => SceneManager.GetSceneAt(0);
    public string CurrentSceneName => SceneManager.GetSceneAt(0).name;
    public SceneTransitor CurrentSceneTransitor => loadedScenes[CurrentScene];
}
