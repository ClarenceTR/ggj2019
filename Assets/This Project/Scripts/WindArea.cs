﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindArea : MonoBehaviour
{
    [Range(-1000f,1000f)] public float forceX;
    [Range(-1000f, 1000f)] public float forceY;
    [SerializeField] Rigidbody2D sheepRB;

    private void Start()
    {
        sheepRB = GameObject.Find("Sheep").GetComponent<Rigidbody2D>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            sheepRB.AddForce(Vector2.right * forceX + Vector2.up * forceY);
        }
    }
}
