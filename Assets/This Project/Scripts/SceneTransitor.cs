﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Luminosity.IO;
using System;

public class SceneTransitor : MonoBehaviour
{
    public float fadeDuration = 0.5f;
    public float fadeDelay = 0f;
    public string nextScene = "SampleScene";

    [SerializeField] CanvasGroup m_maskGroup;
    bool m_sceneTransitionOver;
    public bool IsSceneTransitionOver { get => m_sceneTransitionOver; }

    bool m_fadingAnimationPlaying = false;

    void Start()
    {
        m_sceneTransitionOver = false;
        if (m_maskGroup != null)
        {
            m_maskGroup.alpha = 1;
            m_maskGroup.interactable = true;
            m_maskGroup.blocksRaycasts = true;
        }

        FadeInTheScene();
    }

    public void FadeInTheScene()
    {
        StartCoroutine(C_FadeInTheScene());
    }

    IEnumerator C_FadeInTheScene()
    {
        yield return new WaitForSeconds(fadeDelay);

        // Fade in the scene.
        m_fadingAnimationPlaying = true;
        m_maskGroup.DOFade(0, fadeDuration).OnStart(delegate () { }).OnComplete(delegate ()
        {
            m_fadingAnimationPlaying = false;
            m_sceneTransitionOver = true;
            m_maskGroup.interactable = false;
            m_maskGroup.blocksRaycasts = false;
        });
    }

    public void FadeOutTheScene(Action OnFadingComplete)
    {
        StartCoroutine(C_FadeOutTheScene(OnFadingComplete));
    }

    IEnumerator C_FadeOutTheScene(Action OnFadingComplete)
    {
        yield return new WaitForSeconds(fadeDelay);

        m_fadingAnimationPlaying = true;
        m_maskGroup.interactable = true;
        m_maskGroup.blocksRaycasts = true;

        m_maskGroup.DOFade(1, fadeDuration).OnComplete(delegate ()
        {
            OnFadingComplete();
            m_fadingAnimationPlaying = false;
        });
    }

    void Update()
    {

    }
}
