﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Luminosity.IO;

public class CharacterController2D : MonoBehaviour
{
    [Header("Movement")]
    [Range(0, 100)]
    public float normalSpeed = 7.8f;
    [Range(0, 50)]
    public float slowSpeed = 3.5f;
    [Range(0, 50)]
    public float coldSpeed = 3.5f;
    public bool spriteFacingRight = false;
    [Tooltip("Above which speed the sheep will change facing direction.")]
    public float facingVelocityThreshold = 0.1f;

    [Header("Jump & Ground Check")]
    [Range(0, 100)]
    public float jumpForce = 100f;
    public Transform groundCheck;
    public float checkRadius = 0.1f;
    public LayerMask groundLayer = 1 << 8;
    public int extraJump = 1;
    public float jumpPeriod = 1f;

    [Header("Jump & Ground Check")]
    public Transform hand;

    Rigidbody2D rb;
    Animator anim;
    float h;    // The move input for the horizontal axis.
    float m_speed = 1f;
    bool m_facingRight = false;
    bool m_isGrounded = true;
    float m_extraJump = 0;
    float m_jumpPeriodTimer;
    bool m_jumpable = true;
    bool m_enableInput = true;
    [SerializeField] bool m_autoFacing = true;

    public bool FacingRight
    {
        get => m_facingRight;
        set
        {
            m_facingRight = value;
            Vector3 scaler = transform.localScale;
            scaler.x = Mathf.Abs(scaler.x) * (spriteFacingRight ^ m_facingRight ? -1 : 1);
            transform.localScale = scaler;
        }
    }

    public bool EnableInput { get => m_enableInput;
        set => m_enableInput = value; }

    public bool AutoFacing { get => m_autoFacing; set => m_autoFacing = value; }

    /// <summary>
    /// Put a gameobject under the hand. 
    /// </summary>
    /// <param name=""></param>
    /// <param name=""></param>
    public void Grab(Transform obj)
    {
        obj.parent = hand;
        obj.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Put the gameobject grabbing back to the hierarchy.
    /// </summary>
    public void HandsOff()
    {
        foreach (Transform obj in hand.transform)
        {
            obj.parent = null;
        }
    }

    public void Move(bool right)
    {
        // Should be called every frame when input is disabled.
        if (EnableInput) return;
        var v = new Vector2((right ? 1 : -1) * m_speed, rb.velocity.y);
        rb.velocity = v;
        anim.SetFloat("Speed", v.x);
    }

    public void StopMoving()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        anim.SetFloat("Speed", 0);
    }

    public float Speed { get => m_speed; set => m_speed = value; }

    public bool Jumpable { get => m_jumpable; set => m_jumpable = value; }

    public bool IsKinematic
    {
        set
        {
            rb.isKinematic = value;
            // Reset the velocity.
            if (value) rb.velocity = Vector2.zero;
        }
    }

    public bool IsGrounded { get => m_isGrounded; }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        m_extraJump = extraJump;
        Speed = normalSpeed;
    }

    bool m_inputJumpDown = false;
    bool m_inputJump = false;
    bool m_inputJumpUp = false;
    private void Update()
    {
        if (EnableInput)
        {
            h = InputManager.GetAxis("Horizontal");
            m_inputJumpDown = InputManager.GetButtonDown("Jump");
            m_inputJump = InputManager.GetButton("Jump");
            m_inputJumpUp = InputManager.GetButtonUp("Jump");
        }
    }

    private void FixedUpdate()
    {
        if (EnableInput)
        {
            // Check horizontal move.
            rb.velocity = new Vector2(h * m_speed, rb.velocity.y);

            float animHSpeed = Mathf.Abs(h * m_speed);
            anim.SetFloat("Speed", animHSpeed);

            // Check jump.
            if (Jumpable)
            {
                m_isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, groundLayer);
                anim.SetBool("Grounded", IsGrounded);
                if (IsGrounded) m_extraJump = extraJump;

                if (m_inputJumpDown)
                {
                    if (m_extraJump > 0)
                    {
                        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                        m_jumpPeriodTimer = jumpPeriod;
                        m_extraJump -= 1;
                    }
                    else if (m_extraJump == 0 && IsGrounded)
                    {
                        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                        m_jumpPeriodTimer = jumpPeriod;
                    }
                }

                if (m_jumpPeriodTimer > 0 && m_inputJump)
                {
                    // Renew the velocity so that the player jumps higher when holding the button.
                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                }
                if (m_inputJumpUp)
                {
                    m_jumpPeriodTimer = 0;
                }
                m_jumpPeriodTimer -= Time.deltaTime;
            }

            // Debug
            //ScreenMsgPrinter.Instance.Print("Speed: " + Speed);
        }

        // Set facing
        if (AutoFacing)
        {
            float v = rb.velocity.x;
            if (Mathf.Abs(v) > facingVelocityThreshold)
                FacingRight = v > 0;

            // Debug
            //ScreenMsgPrinter.Instance.Print("Speed: " + rb.velocity);
        }
    }
}
