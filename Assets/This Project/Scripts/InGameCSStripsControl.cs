﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameCSStripsControl : MonoBehaviour
{
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Show()
    {
        anim.SetTrigger("Show");
    }

    public void Hide()
    {
        anim.SetTrigger("Hide");
    }
}
