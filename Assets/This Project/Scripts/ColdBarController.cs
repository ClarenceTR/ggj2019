﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColdBarController : Singleton<ColdBarController>
{
    [SerializeField] ColdBar coldBarGirl;
    [SerializeField] ColdBar coldBarSheep;
    [SerializeField] Image whiteMask;

    Girl m_girl;
    bool m_active = true;
    bool m_evaluateGirl;
    bool m_evaluateSheep;

    public bool Active
    {
        get => m_active;
        set
        {
            m_active = value;
            if (m_evaluateGirl) coldBarGirl.Active = value;
            if (m_evaluateSheep) coldBarSheep.Active = value;
        }
    }

    public bool EvaluateGirl { get => m_evaluateGirl; set { m_evaluateGirl = (coldBarGirl == null) ? false : value; } }
    public bool EvaulateSheep { get => m_evaluateSheep; set { m_evaluateSheep = (coldBarSheep == null) ? false : value; } }

    private void Start()
    {
        m_girl = FindObjectOfType<Girl>();
        m_evaluateGirl = coldBarGirl != null;
        m_evaluateSheep = coldBarSheep != null;
    }

    private void Update()
    {
        if (!m_active) return;
        Evaluate();
    }

    void Evaluate()
    {
        // Read bars. 
        // Update girl states.
        if (m_evaluateGirl)
        {
            // Girl Cold?
            if (coldBarGirl.FeelsCold)
            {
                if (m_girl.State != GirlState.HoldingAndCold &&
                    m_girl.State != GirlState.Cold)
                {
                    if (m_girl.State == GirlState.Holding)
                        m_girl.State = GirlState.HoldingAndCold;
                    else
                        m_girl.State = GirlState.Cold;
                }
            }
            else
            {
                if (m_girl.State != GirlState.Holding &&
                    m_girl.State != GirlState.Normal)
                {
                    if (m_girl.State == GirlState.HoldingAndCold)
                        m_girl.State = GirlState.Holding;
                    else
                        m_girl.State = GirlState.Normal;
                }
            }

            // Girl died?
            if (coldBarGirl.IsDead)
            {
                // If girl died, call level manager GameOver()
                //print("Game Over.");
                LevelManager.Instance.GameOver();
            }
            // Girl dying?
            else if (coldBarGirl.IsDying)
            {
                // set the dying degree as the alpha of the white mask and the snow.
                Color c = whiteMask.color; c.a = coldBarGirl.DyingDegree; whiteMask.color = c;
                //print($"Current alpha: {coldBarGirl.DyingDegree}");
            }

        }

        // Update sheep states.
        // Now nothing.
    }

    public bool MutualWarmed
    {
        set
        {
            if (m_evaluateGirl) coldBarGirl.IsWarmedBySheep = value;
            if (m_evaluateSheep) coldBarSheep.IsWarmedBySheep = value;
        }
    }

    public bool TorchWarmsGirl
    {
        set
        {
            coldBarGirl.IsWarmedByTorch = value;
        }
    }

    public bool TorchWarmsSheep
    {
        set
        {
            coldBarSheep.IsWarmedByTorch = value;
        }
    }
}
