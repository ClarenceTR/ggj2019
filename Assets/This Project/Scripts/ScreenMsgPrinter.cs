﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScreenMsgPrinter : Singleton<ScreenMsgPrinter>
{
    TextMeshProUGUI text;

    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    public void Print(string text)
    {
        this.text.text = text;
    }
}
