﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColdControl : MonoBehaviour
{

    #region Parameters

    public float maxHP = 100;
    public float HPRecoverSpeed = 10f;
    public float HPLoseSpeed = 3f;
    #endregion

    #region Status
    public bool isWarm = false;
    public float currentHP = 1;
    public bool isAlive = true;
    #endregion


    #region References
    public GameObject player;
    [SerializeField] GameObject coldBar;

    #endregion



    void Start()
    {
        //Find Reference
        player = GameObject.Find("Girl");
        if (player == null)
        {
            Debug.Log("Camera cannot find the girl");
        }
        coldBar = GameObject.Find("ColdBar");


        //initialization
        currentHP = maxHP;


    }

    void Update()
    {
        HPCalculating();
        AliveCheck();
    }


    private void AliveCheck()
    {
        if (currentHP < 0)
        {
            if (isAlive)
            {
                isAlive = false;
                //player.GetComponent<PlayerMovement>().enabled = false;
                //player.GetComponent<SheepCommand>().enabled = false;
                player.GetComponent<Collider2D>().enabled = false;
            }
        }
    }

    private void HPCalculating()
    {
        if (isAlive)
        {
            if (isWarm)
            {
                if (currentHP <= maxHP)
                {
                    currentHP += HPRecoverSpeed * Time.deltaTime;
                }
            }
            else
            {
                currentHP -= HPLoseSpeed * Time.deltaTime;
            }
        }
        float displayRatio = 1 - (currentHP / maxHP);
        if (displayRatio >= 0 && displayRatio <= 1)
        {
            coldBar.transform.localScale = new Vector3(displayRatio, 1, 1);
        }

    }
}
