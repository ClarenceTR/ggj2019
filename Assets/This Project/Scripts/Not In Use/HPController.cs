﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPController : Singleton<HPController>
{
    [SerializeField] Image hpBar;

    public float maxHP = 100f;
    [Range(0, 10)]
    public float recoverSpeed = 10f;
    [Range(0, 10)]
    public float loseSpeed = 3f;

    float m_HP;
    bool m_isWarm = false;

    private void Start()
    {
        m_HP = maxHP;
        Display();
    }

    private void Update()
    {
        Evaluate();
        Display();
    }

    void Evaluate()
    {
        if (IsWarm)
        {
            Health += recoverSpeed * Time.deltaTime;
        }
        else
        {
            Health = Health - loseSpeed * Time.deltaTime;
        }
    }

    void Display()
    {
        float ratio = m_HP / maxHP;
        hpBar.fillAmount = ratio;
    }

    public float Health
    {
        get => m_HP;
        set
        {
            m_HP = Mathf.Clamp(value, 0, maxHP);
        }
    }

    public bool IsAlive { get { return m_HP > 0; } }

    public bool IsWarm { get => m_isWarm; set => m_isWarm = value; }
}
