﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    [SerializeField] [Range(0,1)] float cameraFollowingSpeed = 0.1f;
    [SerializeField] [Range(0, 8)] float cameraXoffset = 3.0f;
    [SerializeField] GameObject player;

    void Start()
    {
        player = GameObject.Find("Girl");
        if (player != null)
        {
            
        }
        else
        {
            Debug.Log("Camera cannot find the girl");
        }


    }


    void Update()
    {
        transform.position = new Vector2(Mathf.Lerp(transform.position.x, player.transform.position.x + cameraXoffset, cameraFollowingSpeed),
                                           Mathf.Lerp(transform.position.y, player.transform.position.y, cameraFollowingSpeed));
    }
}
