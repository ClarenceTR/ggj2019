﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameOverSceneController : Singleton<GameOverSceneController>
{
    public float fadeDuration = 0.5f;
    [SerializeField] CanvasGroup whiteMask;
    [SerializeField] CanvasGroup blackMask;
    [SerializeField] Button btnRetry;
    [SerializeField] Button btnMainMenu;

    SceneTransitionController sceneTrans;
    bool m_fadingOver;
    public bool IsFadingOver { get => m_fadingOver; }

    private void Start()
    {
        // Find scene trasition controller.
        sceneTrans = LevelManager.Instance?.sceneTrans;

        // Bind the buttons.
        btnRetry.onClick.AddListener(ReloadCurrentScene);
        btnMainMenu.onClick.AddListener(GoToMainMenu);

        // Fade in the scene.
        m_fadingOver = false;
        if (whiteMask != null)
        {
            whiteMask.alpha = 1;
            whiteMask.interactable = true;
            whiteMask.blocksRaycasts = true;
            whiteMask.DOFade(0, fadeDuration).OnComplete(delegate ()
            {
                m_fadingOver = true;
                whiteMask.interactable = false;
                whiteMask.blocksRaycasts = false;
            });
        }
    }

    void ReloadCurrentScene()
    {
        sceneTrans.GoToScene(sceneTrans.CurrentSceneName);
        FadeOut();
    }

    void GoToMainMenu()
    {
        sceneTrans.GoToScene("MainScreen");
        FadeOut();
    }

    void FadeOut()
    {
        // Black Mask fades in.
        if (blackMask != null)
        {
            blackMask.alpha = 0;
            blackMask.interactable = false;
            blackMask.blocksRaycasts = false;
            blackMask.DOFade(1, fadeDuration).OnComplete(delegate ()
             {
                 blackMask.interactable = true;
                 blackMask.blocksRaycasts = true;
             });
        }
    }
}
