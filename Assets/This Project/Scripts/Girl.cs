﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GirlState
{
    Normal,
    Holding,
    Cold,
    HoldingAndCold,
}

public class Girl : MonoBehaviour
{
    Animator anim;
    CharacterController2D cc;
    ColdBarController m_coldBarController;
    GirlState m_state;

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        cc = GetComponent<CharacterController2D>();
        m_coldBarController = FindObjectOfType<ColdBarController>();
    }

    private void Update()
    {
        // Debug: Press j to show the dialog box. Press Enter or click the blue arrow to go next.
        //if (Input.GetKeyDown(KeyCode.J))
        //{
        //    DialogController d = DialogController.Instance;
        //    string str = "Sunt dolor inventore ab pariatur. Consectetur est rem eligendi maxime voluptas. Et sint sint inventore illum autem aliquid.";
        //    d.Show(str, "Cecilie", "Cecilie");
        //}

        // Debug
        //ScreenMsgPrinter.Instance.Print("State: " + m_state.ToString());
    }

    public GirlState State
    {
        get => m_state;
        set
        {
            m_state = value;
            switch (m_state)
            {
                case GirlState.Normal:
                    cc.Speed = cc.normalSpeed;
                    cc.Jumpable = true;
                    anim.SetBool("Cold", false);
                    // Tell the hp controller that it's no longer warm.
                    m_coldBarController.MutualWarmed= false;
                    break;

                case GirlState.Holding:
                    cc.Speed = cc.slowSpeed;
                    cc.Jumpable = false;
                    anim.SetBool("Cold", false);
                    // Tell the hp controller that it's warm now.
                    m_coldBarController.MutualWarmed = true;
                    break;

                case GirlState.Cold:
                    cc.Speed = cc.coldSpeed;
                    cc.Jumpable = true;
                    anim.SetBool("Cold", true);
                    break;

                case GirlState.HoldingAndCold:
                    cc.Speed = cc.slowSpeed;
                    cc.Jumpable = false;
                    anim.SetBool("Cold", true);
                    break;

                default:
                    break;
            }
        }
    }
}
