﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Luminosity.IO;
using TMPro;

public class IndicatorShower : Singleton<IndicatorShower>
{
    [Header("Calling Indicator")]
    [Tooltip("The calling sheep visual effect when calls a sheep to follow you.")]
    public GameObject pf_vfxCalling;
    public LayerMask groundLayer = 1 << 8;
    

    [SerializeField] Transform indicatorPlace;  // The transform under this gameobject to decide the position of the indicator.

    [SerializeField] GameObject m_indicator;
    [SerializeField] GameObject m_keyPrompt;
    [SerializeField] GameObject m_callIndicator;
    Animator animKeyPrompt;
    bool m_shownKeyPrmopt;

    private void Start()
    {
        animKeyPrompt = m_keyPrompt.GetComponentInChildren<Animator>();
    }

    public void ShowCallingVFX()
    {
        Instantiate(pf_vfxCalling, m_callIndicator.transform.position, pf_vfxCalling.transform.rotation);
    }

    public void ShowIndicator()
    {
        if (!m_shownKeyPrmopt && !DuringAnimation)
        {
            SetIndicatorPosition();
            ShowKeyPrompt();
        }
        else
        {
            // Update the position.
            SetIndicatorPosition();
        }
    }

    private void SetIndicatorPosition()
    {
        RaycastHit2D hit = Physics2D.Raycast(indicatorPlace.position, Vector2.down, float.MaxValue, groundLayer);
        if (hit) m_indicator.transform.position = new Vector3
                (
                  indicatorPlace.position.x,
                  hit.point.y,
                  m_indicator.transform.position.z
                );
    }

    public void HideIndicator()
    {
        if (m_shownKeyPrmopt && !DuringAnimation)
        {
            HideKeyPrompt();
        }
    }

    public bool DuringAnimation
    {
        get => m_showingAnimationPlaying | m_hidingAnimationPlaying;
    }

    bool m_showingAnimationPlaying = false;
    public void ShowKeyPrompt()
    {
        animKeyPrompt.SetTrigger("Show");
        m_showingAnimationPlaying = true;
    }

    bool m_hidingAnimationPlaying = false;
    public void HideKeyPrompt()
    {
        animKeyPrompt.SetTrigger("Hide");
        m_hidingAnimationPlaying = true;
    }

    public void OnShownKeyPrompt()
    {
        m_shownKeyPrmopt = true;
        m_showingAnimationPlaying = false;
    }

    public void OnHiddenKeyPrompt()
    {
        m_indicator.transform.localPosition = Vector3.zero;
        m_shownKeyPrmopt = false;
        m_hidingAnimationPlaying = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(indicatorPlace.position, Vector2.down);
    }
}
