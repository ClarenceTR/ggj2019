﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelTemplate : MonoBehaviour
{
    protected LevelManager m_manager;

    // Should be set by LevelManager during initialization.
    public LevelManager Manager { set => m_manager = value; }
    public abstract void StartLevel();
}
