﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;
using Luminosity.IO;
using DG.Tweening;

public class Level_0 : LevelTemplate
{
    /// <summary>
    /// Called at the end of initialization by LevelManager.
    /// </summary>
    public override void StartLevel()
    {
        // Enter Cutscene. 
        m_manager.EnterCutScene();

        // Girl moves right.
        c_moveGirl = StartCoroutine(C_MoveGirl(true));
    }

    Coroutine c_moveGirl;
    IEnumerator C_MoveGirl(bool right)
    {
        while (true)
        {
            m_manager.ccGirl.Move(right);

            yield return new WaitForEndOfFrame();
        }
    }

    public void AfterGirlEnter(LevelTrigger levelTrigger, Collider2D collider)
    {
        // Disable the trigger.
        levelTrigger.gameObject.SetActive(false);

        // Stop the girl and the sheep.
        StartCoroutine(C_StopGirlMoveAfter(1f));

        // Girl says stuff.
        m_manager.dialogController.Lines = levelTrigger.lines;
        m_manager.dialogController.Next();

        // Wait for the play to hit next to call LookAtCameraAndMoveOn()
    }

    IEnumerator C_StopGirlMoveAfter(float duration)
    {
        yield return new WaitForSeconds(duration);

        StopCoroutine(c_moveGirl);
        m_manager.ccGirl.StopMoving();
    }

    public void AfterGirlEnterJump(LevelTrigger levelTrigger, Collider2D collider)
    {
        // Disable this trigger.
        levelTrigger.gameObject.SetActive(false);

        // Say jump prompt.
        m_manager.dialogController.Lines = levelTrigger.lines;
        m_manager.dialogController.Next();
    }

    public void AfterGirlEnterWarm(LevelTrigger levelTrigger, Collider2D collider)
    {
        // Disable this trigger.
        levelTrigger.gameObject.SetActive(false);

        // Disable input until hit next.
        m_manager.EnabledInput = false;
        
        // Say warmed up prompt.
        m_manager.dialogController.Lines = levelTrigger.lines;
        m_manager.dialogController.Next();

        // After event is added in the Unity Editor.

        // Highlight the cold bar.
        Highlight();

    }

    public CinemachineVirtualCamera torchCamera;
    public CinemachineVirtualCamera startCamera;

    float torchCameraTimer = 0f;
    float torchCameraDuration = 2f;

    public void LookAtCameraAndMoveOn()
    {
        // After hitting next, camera goes to the torch, 1 seconds after or key hits, camera goes back to the start camera.
        StartCoroutine(C_LookAtCameraAndMoveOn());
    }

    IEnumerator C_LookAtCameraAndMoveOn()
    {
        // Camera blends to the torch camera.
        torchCamera.enabled = true;
        // Wait until time is over or next key is pressed.
        yield return new WaitUntil(() => ((torchCameraTimer += Time.deltaTime) > torchCameraDuration));
        // Camera switches back.
        torchCamera.enabled = false;

        // Exit cutscene, but still disabled input.
        ExitCutScene();

        // Camera swtiching back offset
        yield return new WaitForSeconds(1.5f);

        // Tutorial shows the bar
        DialogLine line = new DialogLine()
        {
            text = "This bar shows how cold you are.",
            speaker = string.Empty,
            avatarImgKey = string.Empty,
            OnAfterDisplay = new UnityEvent(),
        };
        // After the player ends the dialog, exit the cutscene.
        line.OnAfterDisplay.AddListener(EnableInput);
        // Display tutorial dialog.
        m_manager.dialogController.Lines = new DialogLine[] { line };
        m_manager.dialogController.Next();
        // Display highlight
        Highlight();
    }


    [SerializeField] SpriteRenderer highlight;
    [SerializeField] SpriteRenderer mask;
    public void Highlight()
    {
        // Show mask.
        // 0 to amount
        mask.DOFade(155f/255f, 0.5f);

        // Show highlight.
        // 0 to amount
        highlight.DOFade(189f/255f, 0.5f);

    }

    public void Dehighlight()
    {
        // Hide mask and highlight.
        mask.DOFade(0, 0.5f);
        highlight.DOFade(0, 0.5f);

    }

    public void ExitCutScene()
    {
        // Cut scene disables.
        m_manager.ExitCutScene(false);
    }

    public void EnableInput()
    {
        m_manager.EnabledInput = true;

        // Turn off highlight.
        Dehighlight();

        // Camera goes to the game camera.
        startCamera.enabled = false;
    }

    public void PassLevel()
    {
        m_manager.sceneTrans.GoToNextScene();
    }
}
