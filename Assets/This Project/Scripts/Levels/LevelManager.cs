﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Luminosity.IO;

[RequireComponent(typeof(LevelTemplate))]
public class LevelManager : Singleton<LevelManager>
{
    // External members don't have "m_" prefix.
    public MusicController musicController;
    public CharacterController2D ccGirl;
    public SheepController sheep;
    public InGameCSStripsControl uiStrips;
    public LevelTemplate currentLevel;
    public DialogController dialogController;
    public SceneTransitionController sceneTrans;
    public ColdBarController coldController;

    public bool ObjectsRegistered;
    public bool Initialized;

    public bool EnabledInput
    {
        get => m_enabledInput;
        set
        {
            m_enabledInput = value;
            ccGirl.EnableInput = m_enabledInput;
            // Also stops girl's moving.
            ccGirl.StopMoving();
            if (sheep) sheep.EnableInput = m_enabledInput;
        }
    }

    public bool GamePaused
    {
        get => m_gamePaused;
        set
        {
            m_gamePaused = value;

            coldController.Active = !value;
        }
    }

    bool m_enabledInput;
    bool m_gamePaused;

    private void Start()
    {
        // Initialization.
        StartCoroutine(C_Initialize());
    }

    private void Update()
    {
        // Press Esc to go back to main menu.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            var musicController = MusicController.Instance;
            musicController?.Play("MainMenu");
            if (musicController) musicController.volume = 0.6f;
            sceneTrans?.GoToScene("MainScreen");
        }
    }

    IEnumerator C_Initialize()
    {
        yield return StartCoroutine(C_RegisterObjects());

        // Music control.
        musicController?.Play("Level01");
        if (musicController) musicController.volume = 0.3f;

        // Start current Level.
        currentLevel.StartLevel();

        Initialized = true;
    }

    float m_initializationTimeLimit = 0.5f;
    float m_initializationTimer;

    IEnumerator C_RegisterObjects()
    {
        currentLevel = GetComponent<LevelTemplate>();
        if (currentLevel) currentLevel.Manager = this;
        yield return new WaitUntil(() => dialogController = DialogController.Instance);
        yield return new WaitUntil(() => ccGirl = FindObjectOfType<CharacterController2D>());
        yield return new WaitUntil(() => uiStrips = FindObjectOfType<InGameCSStripsControl>());
        yield return new WaitUntil(() => sceneTrans = SceneTransitionController.Instance);
        yield return new WaitUntil(() => coldController = ColdBarController.Instance);
        // Don't have to find the sheep
        yield return new WaitUntil(() => ((m_initializationTimer += Time.deltaTime) > m_initializationTimeLimit) || (sheep = SheepController.Instance));
        // Don't have to find music controller
        yield return new WaitUntil(() => ((m_initializationTimer += Time.deltaTime) > m_initializationTimeLimit) || (musicController = FindObjectOfType<MusicController>()));

        ObjectsRegistered = true;
    }

    public void EnterCutScene()
    {
        // Show the strips on the top and the bottom.
        uiStrips.Show();

        // Disable the ability to control playermovement.
        EnabledInput = false;

        // Pause the game.
        GamePaused = true;
    }

    public void ExitCutScene(bool enableInput = true)
    {
        // Hide the strips on the top and the bottom.
        uiStrips.Hide();

        // Reenable the ability to control playermovement.
        EnabledInput = enableInput;

        // Resume the game.
        GamePaused = false;
    }

    public void ShowDialog(DialogLine[] lines)
    {
        StartCoroutine(C_ShowDialog(lines));
    }

    IEnumerator C_ShowDialog(DialogLine[] lines)
    {
        if (dialogController.Shown)
        {
            dialogController.Hide();
            yield return new WaitUntil(() => !dialogController.Shown);
        }

        dialogController.Lines = lines;
        dialogController.Next();
    }

    public void GameOver()
    {
        // Todo: Handle different game over type.

        // Turn off game parts.
        GamePaused = true;

        // Disable Input.
        EnabledInput = false;

        sceneTrans.LoadSceneOnTop("GameOver", false);
    }
}
