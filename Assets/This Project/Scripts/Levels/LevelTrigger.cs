﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelTrigger : MonoBehaviour
{
    public DialogLine[] lines;
    [Tooltip("Things to do when triggered.")]
    public TriggerEnterEvent onTriggerEnterEvent;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        // Fire the bound functions in Editor.
        onTriggerEnterEvent.Invoke(this, collider);
    }

    /// <summary>
    /// Show the dialog lines set on this trigger.
    /// </summary>
    public void ShowDialog(LevelTrigger levelTrigger, Collider2D collider)
    {
        var levelManager = LevelManager.Instance;
        if (levelManager)
        {
            levelManager.dialogController.Lines = lines;
            levelManager.dialogController.Next();
        }
    }
}

[Serializable]
public class TriggerEnterEvent : UnityEvent<LevelTrigger, Collider2D>
{
    
}
