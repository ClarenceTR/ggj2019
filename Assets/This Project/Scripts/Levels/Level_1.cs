﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Cinemachine;
using Luminosity.IO;
using DG.Tweening;

public class Level_1 : LevelTemplate
{
    public CinemachineVirtualCamera startCamera;
    public CinemachineVirtualCamera followCamera;
    public CinemachineVirtualCamera lucasTrailCamera;
    public string cutSceneName = "C_level_1";
    public ColdBar coldBarSheep;

    bool m_foundLucas;

    public override void StartLevel()
    {
        // Girl moves right.
        c_moveGirl = StartCoroutine(C_MoveGirl(true));

        // Disable input and pause game.
        m_manager.GamePaused = true;
        m_manager.EnabledInput = false;

        // Hide the Lucas Bar.
        coldBarSheep.gameObject.SetActive(false);
        m_manager.coldController.EvaulateSheep = false;
    }

    Coroutine c_moveGirl;
    IEnumerator C_MoveGirl(bool right)
    {
        while (true)
        {
            m_manager.ccGirl.Move(right);

            yield return new WaitForEndOfFrame();
        }
    }

    public void AfterGirlEnter(LevelTrigger levelTrigger, Collider2D collider)
    {
        // Disable the trigger.
        levelTrigger.gameObject.SetActive(false);

        // Stop the girl and the sheep.
        StartCoroutine(C_StopGirlMoveAfter(1f));

        // Girl says stuff.
        m_manager.ShowDialog(levelTrigger.lines);

        // Wait for player hits next and call EnableInput();
    }

    IEnumerator C_StopGirlMoveAfter(float duration)
    {
        yield return new WaitForSeconds(duration);

        StopCoroutine(c_moveGirl);
        m_manager.ccGirl.StopMoving();
    }

    public void CameraTurnsToLucasTrail(LevelTrigger levelTrigger, Collider2D collider)
    {
        StartCoroutine(C_CameraTurnsToLucasTrail(levelTrigger, collider));
    }

    float lucasTrailCameraTimer = 0f;
    float lucasTrailCameraDuration = 2f;
    IEnumerator C_CameraTurnsToLucasTrail(LevelTrigger levelTrigger, Collider2D collider)
    {
        // Disable the trigger.
        levelTrigger.gameObject.SetActive(false);

        lucasTrailCamera.enabled = true;
        m_manager.EnabledInput = false;
        m_manager.GamePaused = true;

        // Go to that trial. After lucasTrailCameraDuration seconds, go back and at the same time show the dialog.
        yield return new WaitUntil(() => ((lucasTrailCameraTimer += Time.deltaTime) > lucasTrailCameraDuration));
        lucasTrailCamera.enabled = false;
        // Girl says stuff.
        int lineLen = levelTrigger.lines.Length;
        m_manager.ShowDialog(levelTrigger.lines);

        // Don't enable input until next is hit.
        // This is set in the editor.
    }

    public void AfterGirlAboutToLeaveVillage(LevelTrigger levelTrigger, Collider2D collider)
    {
        if (!m_foundLucas)
        {
            // Girl says stuff.
            m_manager.ShowDialog(levelTrigger.lines);
        }

        // This dialog trigger is repeatable. So don't disable the trigger.
    }

    public void AfterGirlReachesToLucas(LevelTrigger levelTrigger, Collider2D collider)
    {
        // If it's not the girl who triggers the trigger, ignore.
        if (!collider.CompareTag("Player")) return;

        // Disable the trigger.
        levelTrigger.gameObject.SetActive(false);

        // Bind OnLoad.
        m_manager.sceneTrans.OnLoadTopScene.AddListener(AfterLucasCutSceneLoaded);
        // Bind OnUnload.
        m_manager.sceneTrans.OnUnloadTopScene.AddListener(AfterLucasCutSceneOver);
        // Load the cutscene.
        m_manager.sceneTrans.LoadSceneOnTop(cutSceneName);
    }

    void AfterLucasCutSceneLoaded()
    {
        // Disable the input and pause the game.
        m_manager.EnabledInput = false;
        m_manager.GamePaused = true;
    }

    public void AfterLucasCutSceneOver()
    {
        // unbind the bound OnLoad().
        m_manager.sceneTrans.OnLoadTopScene.RemoveListener(AfterLucasCutSceneLoaded);

        // unbind the bound OnUnload().
        m_manager.sceneTrans.OnUnloadTopScene.RemoveListener(AfterLucasCutSceneOver);

        // Fade in the scene.
        m_manager.sceneTrans.CurrentSceneTransitor.FadeInTheScene();

        // Set status.
        m_foundLucas = true;
        ShowLucasBar();
    }

    public void ShowLucasBar()
    {
        // Enable the Lucas Bar.
        coldBarSheep.gameObject.SetActive(true);

        // Girl says stuff.
        DialogLine line1 = new DialogLine()
        {
            text = "Lucas, we need to leave our village to find a warm place.",
            speaker = "Cecilie",
            avatarImgKey = "Cecilie",
            OnAfterDisplay = new UnityEvent(),
        };
        // After hits next, highlight Lucas bar.
        line1.OnAfterDisplay.AddListener(Highlight);
        DialogLine line2 = new DialogLine()
        {
            text = "Lucas also needs to stay warm.",
            speaker = string.Empty,
            avatarImgKey = string.Empty,
            OnAfterDisplay = new UnityEvent(),
        };
        // After hits next, dehighlight, which will trigger the teaching lucas part.
        line2.OnAfterDisplay.AddListener(Dehighlight);
        // Display the dialogs.
        m_manager.dialogController.Lines = new DialogLine[] { line1, line2 };
        m_manager.dialogController.Next();


    }

    [SerializeField] SpriteRenderer highlight;
    //[SerializeField] SpriteRenderer mask;
    [SerializeField] RawImage mask;
    public void Highlight()
    {
        float duration = 1f;
        // Show mask.
        // 0 to amount
        mask.DOFade(185f / 255f, duration);

        // Show highlight.
        // 0 to amount
        highlight.DOFade(189f / 255f, duration);

    }

    public void Dehighlight()
    {
        // Hide masks and highlight.
        float duration = 0.5f;
        mask.DOFade(0, duration);
        highlight.DOFade(0, duration).OnComplete(TeachCallingLucas);
    }

    Transform followCamTarget = null;
    void TeachCallingLucas()
    {
        // Activate the Lucas bar.
        m_manager.coldController.EvaulateSheep = true;

        // Girl says stuff.
        DialogLine line = new DialogLine()
        {
            text = "Let’s leave our village, Lucas. (Press [E] and Lucas will go to where you are.)",
            speaker = "Cecilie",
            avatarImgKey = "Cecilie",
            OnAfterDisplay = new UnityEvent(),
        };
        // After that, girl moves right. Camera remains still.
        line.OnAfterDisplay.AddListener(delegate ()
        {
            followCamTarget = followCamera.Follow;
            followCamera.Follow = null;
            c_moveGirl = StartCoroutine(C_MoveGirl(true));
        });

        // Re-enable the input and resume the game.
        m_manager.EnabledInput = true;
        m_manager.GamePaused = false;
    }

    public void EnableInput()
    {
        // Enable input.
        m_manager.EnabledInput = true;
        m_manager.GamePaused = false;

        // Switch to game camera.
        startCamera.enabled = false;
    }

    public void EnableInputAfterCameraTurnsToLucasTrail()
    {
        m_manager.EnabledInput = true;
        m_manager.GamePaused = false;
    }
}
