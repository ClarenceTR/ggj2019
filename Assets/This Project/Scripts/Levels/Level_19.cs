﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;
using Luminosity.IO;

public class Level_19 : LevelTemplate
{
    /// <summary>
    /// Called at the end of initialization by LevelManager.
    /// </summary>
    public override void StartLevel()
    {
        // Enter Cutscene. 
        m_manager.EnterCutScene();

        // Girl and the sheep move right.
        c_moveGirlSheep = StartCoroutine(C_MoveGirlSheep(true));
    }

    Coroutine c_moveGirlSheep;
    IEnumerator C_MoveGirlSheep(bool right)
    {
        while (true)
        {
            m_manager.ccGirl.Move(right);
            m_manager.sheep.Move(right);

            yield return new WaitForEndOfFrame();
        }
    }

    public void AfterGirlSheepEnter(LevelTrigger levelTrigger, Collider2D collider)
    {
        // Disable the trigger.
        levelTrigger.gameObject.SetActive(false);

        // Stop the girl and the sheep.
        StartCoroutine(C_StopGirlSheepMoveAfter(1f));

        // Girl says: Lucas, there's a torch ahead. Let's go and keep warm.
        m_manager.dialogController.Lines = levelTrigger.lines;
        m_manager.dialogController.Next();

        // Wait for the play to hit next to call LookAtCameraAndMoveOn()
    }

    IEnumerator C_StopGirlSheepMoveAfter(float duration)
    {
        yield return new WaitForSeconds(duration);

        StopCoroutine(c_moveGirlSheep);
        m_manager.ccGirl.StopMoving();
        m_manager.sheep.StopMoving();
    }

    public CinemachineVirtualCamera torchCamera;
    public CinemachineVirtualCamera startCamera;

    float torchCameraTimer = 0f;
    float torchCameraDuration = 2f;

    public void LookAtCameraAndMoveOn()
    {
        // After hitting next, camera goes to the torch, 1 seconds after or key hits, camera goes back to the start camera.
        StartCoroutine(C_LookAtCameraAndMoveOn());
    }

    IEnumerator C_LookAtCameraAndMoveOn()
    {
        // Camera blends to the torch camera.
        torchCamera.enabled = true;
        // Wait until time is over or next key is pressed.
        yield return new WaitUntil(() => ((torchCameraTimer += Time.deltaTime) > torchCameraDuration));
        // Camera switches back.
        torchCamera.enabled = false;

        // Camera swtiching back offset
        yield return new WaitForSeconds(1.5f);
        // Lucas says: Mehh...
        DialogLine line = new DialogLine()
        {
            text = "Mehh...",
            speaker = "Lucas",
            avatarImgKey = "Lucas",
            OnAfterDisplay = new UnityEvent(),
        };
        // After the player ends the dialog, exit the cutscene.
        line.OnAfterDisplay.AddListener(ExitCutScene);
        // Display lucas dialog.
        m_manager.dialogController.Lines = new DialogLine[] { line };
        m_manager.dialogController.Next();
    }

    public void ExitCutScene()
    {
        // Cut scene disables.
        m_manager.ExitCutScene();

        // Camera goes to the game camera.
        startCamera.enabled = false;
    }
}
