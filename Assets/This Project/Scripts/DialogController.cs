﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Luminosity.IO;
using System;

public class DialogController : Singleton<DialogController>
{
    public float fadeDuration = 0.5f;
    public float typeWaitingTime = 0.1f;

    [SerializeField] DialogLine[] m_lines;
    [SerializeField] TextMeshProUGUI m_text;
    [SerializeField] TextMeshProUGUI m_title;
    [SerializeField] Image m_avatar;
    [SerializeField] List<string> m_avatarImgKeys;
    [SerializeField] List<Sprite> m_avatarImgValues;
    [SerializeField] Button m_nextButton;

    Animator anim;
    CanvasGroup m_canvasGroup;
    bool m_shown = false;
    int m_curIndex;

    public bool Shown => m_shown;

    public DialogLine[] Lines
    {
        set
        {
            // Cannot set when a line is shown.
            if (m_shown) return;

            m_lines = value;

            // Reset parameters.
            m_curIndex = -1;
        }
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        m_canvasGroup = GetComponent<CanvasGroup>();
        m_curIndex = -1;
    }

    public void Update()
    {
        if (m_shown)
        {
            if (InputManager.GetButtonDown("UI_Submit") || InputManager.GetMouseButtonDown(0))
            {
                m_nextButton.onClick.Invoke();
            }
        }
    }

    bool m_fadingAnimationPlaying = false;
    public void Show(string text, string title = null, string imgKey = null)
    {
        m_fadingAnimationPlaying = true;

        bool titleNotNull = title != null;
        bool imgNotNull = imgKey != null;

        // If title is null, doesn't change title
        if (titleNotNull)
        {
            // If title is empty, doesn't display title.
            // Else, display the name.
            m_title.text = title;
        }
        // If imgKey is null, doesn't change avatar.
        if (imgNotNull)
        {
            // If imgKey is empty, doesn't display image.
            if (imgKey == string.Empty)
            {
                m_avatar.color = Color.clear;
                m_avatar.sprite = null;
            }
            else
            {
                // If there is this key and this value, display the image specified.
                int i = m_avatarImgKeys.IndexOf(imgKey);
                if (i > -1 && i < m_avatarImgValues.Count)
                {
                    m_avatar.color = Color.white;
                    m_avatar.sprite = m_avatarImgValues[i];
                }
                // Else, doesn't change avatar.
            }
        }

        // If the title is null, doesn't fade in.
        if (titleNotNull)
        {
            // Fade in the dialog box.
            m_canvasGroup.DOFade(1, fadeDuration).OnComplete(delegate ()
            {
            // Begin the type animation.
            c_type = StartCoroutine(C_Type(text));

            // Enable the input for the dialog box.
            m_canvasGroup.interactable = true;
                m_canvasGroup.blocksRaycasts = true;

            // Set the state.
            m_shown = true;
                m_fadingAnimationPlaying = false;
            });
        }
        else
        {
            // Doesn't fade in.
            // Begin the type animation.
            c_type = StartCoroutine(C_Type(text));

            // Enable the input for the dialog box.
            m_canvasGroup.interactable = true;
            m_canvasGroup.blocksRaycasts = true;

            // Set the state.
            m_shown = true;
            m_fadingAnimationPlaying = false;
        }
    }

    public void Hide()
    {
        m_fadingAnimationPlaying = true;

        // Fade out the dialog box.
        m_canvasGroup.DOFade(0, fadeDuration).OnComplete(delegate ()
        {
            // Disable the input for the dialog box.
            m_canvasGroup.interactable = false;
            m_canvasGroup.blocksRaycasts = false;

            // Set the state.
            m_shown = false;
            m_title.text = string.Empty;
            m_text.text = string.Empty;
            m_avatar.sprite = null;
            m_fadingAnimationPlaying = false;
        });
    }

    /// <summary>
    /// Hide() and Show()
    /// </summary>
    public void Transit(string text, string title = null, string imgKey = null)
    {
        m_fadingAnimationPlaying = true;

        // If the title dosn't change or is null, don't fade.
        bool noFading = title == null || m_title.text.Equals(title);
        if (noFading)
        {
            // Don't fade.
            Show(text, null, null); // Tell the Show() not to change tilte and img.
        }
        else
        {
            // Fade out the dialog box.
            m_canvasGroup.DOFade(0, fadeDuration).OnComplete(delegate ()
            {
                // Disable the input for the dialog box.
                m_canvasGroup.interactable = false;
                m_canvasGroup.blocksRaycasts = false;

                // Set the state.
                m_shown = false;
                m_title.text = string.Empty;
                m_text.text = string.Empty;
                m_avatar.sprite = null;
                m_fadingAnimationPlaying = false;

                // Show the dialog after hiding is done.
                Show(text, title, imgKey);
            });
        }
    }

    public void Next()
    {
        // If it's typing, skip the typing effect.
        if (m_typingAnimationPlaying)
        {
            SkipTyping();
        }
        else
        {
            if (!m_fadingAnimationPlaying)
            {
                // Invoke the OnAfterDisplay.
                if (m_curIndex < m_lines.Length && m_curIndex >= 0)
                    m_lines[m_curIndex].OnAfterDisplay.Invoke();

                // Go to the next line.
                if (m_curIndex < m_lines.Length - 1)
                {
                    m_curIndex += 1;
                    // Show this line.
                    var line = m_lines[m_curIndex];
                    if (m_shown)
                    {
                        Transit(
                        line.text,
                        line.speaker,
                        line.avatarImgKey
                        );
                    }
                    else
                    {
                        Show(
                        line.text,
                        line.speaker,
                        line.avatarImgKey
                        );
                    }
                }
                else
                {
                    // It's the end, Hide.
                    Hide();
                }
            }
        }
    }

    bool m_typingAnimationPlaying = false;
    Coroutine c_type;
    string m_textToType;
    IEnumerator C_Type(string text)
    {
        m_typingAnimationPlaying = true;
        m_textToType = text;
        string displayText = string.Empty;
        m_text.text = $"<color=#00000000>{m_textToType}</color>";
        for (int i = 0; i < m_textToType.Length; i++)
        {
            yield return new WaitForSeconds(typeWaitingTime);
            displayText += m_textToType[i];
            m_text.text = $"{displayText}<color=#00000000>{m_textToType.Substring(i + 1, m_textToType.Length - i - 1)}</color > ";
        }
        yield return new WaitForSeconds(typeWaitingTime);
        m_text.text = displayText;

        // Reset states.
        m_textToType = null;
        m_typingAnimationPlaying = false;
    }

    void SkipTyping()
    {
        if (m_typingAnimationPlaying)
        {
            // Stop the animtion.
            StopCoroutine(c_type);
            m_typingAnimationPlaying = false;

            // Set the text directly.
            if (m_textToType != null)
                m_text.text = m_textToType;

            // Reset states.
            m_textToType = null;
        }
    }
}

[Serializable]
public class DialogLine
{
    public string text;
    public string speaker;
    public string avatarImgKey;
    public UnityEvent OnAfterDisplay;
}
