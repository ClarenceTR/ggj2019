﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MusicController : MonoBehaviour
{
    public float fadeDuration = 2f;
    public float volume = 0.6f;
    [SerializeField] List<string> m_clipKeys;
    [SerializeField] List<AudioClip> m_clipValues;

    AudioSource audio;
    [SerializeField] string curClipKey = "Sample";

    float m_volume;

    static MusicController m_instance;
    public static MusicController Instance { get => m_instance; }
    private void Awake()
    {
        if (m_instance != null)
        {
            // Duplicate. Destroy meself.
            Destroy(gameObject);
        }
        else
        {
            m_instance = this;
        }
    }

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        m_volume = volume;
        DontDestroyOnLoad(gameObject);
        Play(curClipKey);
    }

    bool m_fadingSFXPlaying = false;

    public void Play(string clipKey)
    {
        Play(clipKey, 0);
    }

    public void Play(string clipKey, float delay)
    {
        if (!m_fadingSFXPlaying)
        {
            if (audio.isPlaying)
            {
                // Transit into next audio
                TransitClip(clipKey, delay);
            }
            else
            {
                PlayClip(clipKey);
            }
        }

    }

    void PlayClip(string clipKey, float delay = 0)
    {
        m_fadingSFXPlaying = true;
        int i = m_clipKeys.IndexOf(clipKey);
        if (i > -1 && i < m_clipValues.Count)
        {
            audio.clip = m_clipValues[i];
        }

        audio.volume = 0;
        audio.Play();
        audio.DOFade(volume, fadeDuration).SetDelay(delay).OnComplete(delegate ()
        {
            // Set states.
            m_fadingSFXPlaying = false;
        });
    }

    public void Stop()
    {
        Stop(fadeDuration);
    }

    public void Stop(float duration)
    {
        m_fadingSFXPlaying = true;
        audio.DOFade(0, duration).OnComplete(delegate ()
        {
            audio.Stop();

            // Set states.
            m_fadingSFXPlaying = false;
        });
    }

    public void TransitClip(string clipKey, float waitTime)
    {
        m_fadingSFXPlaying = true;
        audio.DOFade(0, fadeDuration).OnComplete(delegate ()
        {
            audio.Stop();

            // Set states.
            m_fadingSFXPlaying = false;

            // Start to play the next clip.
            PlayClip(clipKey, waitTime);
        });


    }
}
