﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScreenButtonControl : MonoBehaviour
{
    CanvasTransition canvasTransition;
    [SerializeField] int nextSceneIndex = 1;
    SceneManager sceneManager;

    private void Start()
    {
        canvasTransition = FindObjectOfType<CanvasTransition>();
        canvasTransition.FadeInScene(2f);
    }

    public void LoadNextScene()
    {
        canvasTransition.FadeOutScene(2f);
        StartCoroutine(C_LoadNextScene(2f));
    }

    IEnumerator C_LoadNextScene(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        SceneManager.LoadScene(nextSceneIndex);

    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
