﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroyer : MonoBehaviour {

    public float lifeTime = 2;

    private void Start()
    {
        Destroy(gameObject, lifeTime);
    }
}
