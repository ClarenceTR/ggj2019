﻿/// Author: Clarene Hann. Dec 2018.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Singleton class for any class.
/// To use it, simply inherit this class with it's own type.
/// Example: the DebuggerControl class.
/// </summary>
public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    static T instance;

    public static T Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<T>();
            }
            return instance;
        }
    }
}