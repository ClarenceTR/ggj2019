﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Luminosity.IO;
using UnityEngine.Events;

public class SubtitleController : Singleton<SubtitleController>
{
    public string[] lines;
    public float subtitleFadeDuration = 0.5f;

    [SerializeField] CanvasGroup m_subtitleGroup;
    [SerializeField] TextMeshProUGUI m_text;
    [SerializeField] Button m_nextButton;
    [SerializeField] Image m_cg;
    [SerializeField] List<Sprite> m_cgSprites;    // The background CGs
    [SerializeField] List<int> m_cgIndices;

    Animator animCG;

    int m_curIndex;
    bool m_subtitleShown;
    bool m_cgShown;

    private void Start()
    {
        animCG = GetComponentInChildren<Animator>();
        m_curIndex = -1;
        Next();
    }

    public void Update()
    {
        if (InputManager.GetButtonDown("UI_Submit") || InputManager.GetMouseButtonDown(0))
        {
            m_nextButton.onClick.Invoke();
        }
    }

    public bool DuringAnimation
    {
        get => m_fadingCGAnimationPlaying | m_fadingSubtitleAnimationPlaying;
    }

    public void Next()
    {
        if (!DuringAnimation)
        {
            if (m_curIndex < lines.Length - 1)
            {
                m_curIndex += 1;
                if (m_subtitleShown)
                    FadeIntoSubtitle(lines[m_curIndex]);
                else
                    ShowSubtitle(lines[m_curIndex]);

                // If there's a CG at this line, show CG.
                int i = m_cgIndices.IndexOf(m_curIndex);
                if (i != -1)
                {
                    if (m_cgShown)
                        FadeIntoCG(m_cgSprites[i]);
                    else
                        ShowCG(m_cgSprites[i]);
                }
            }
            else
            {
                // Cut Scene is over.
                onCutSceneOverEvent.Invoke();
            }
        }
    }

    public void OnCutSceneOverDefault()
    {
        // Read the SceneTransionController. If it's multi-scene, then unload the top. Otherwise, go to the next scene.
        var sceneTransit = SceneTransitionController.Instance;
        if (sceneTransit.MultipleScenes)
            sceneTransit.UnloadTopScene();
        else
            sceneTransit.GoToNextScene();
    }

    [Tooltip("Things to do when cut scene is over.")]
    public UnityEvent onCutSceneOverEvent;

    bool m_fadingCGAnimationPlaying = false;
    bool m_fadingSubtitleAnimationPlaying = false;
    void ShowSubtitle(string text)
    {
        m_fadingSubtitleAnimationPlaying = true;
        // To ensure it starts transparent.
        m_subtitleGroup.alpha = 0;
        m_text.text = text;
        m_subtitleGroup.DOFade(1, subtitleFadeDuration).OnComplete(delegate ()
        {
            // Set the state.
            m_subtitleShown = true;
            m_fadingSubtitleAnimationPlaying = false;
        });

    }

    void FadeIntoSubtitle(string text)
    {
        // Hide and show again.
        m_fadingSubtitleAnimationPlaying = true;
        // Hide.
        m_subtitleGroup.DOFade(0, subtitleFadeDuration).OnComplete(delegate ()
        {
            // Set the state.
            m_subtitleShown = false;
            m_fadingSubtitleAnimationPlaying = false;

            // Set the text and fade in.
            ShowSubtitle(text);
        });
    }

    void HideSubtitle()
    {
        m_fadingSubtitleAnimationPlaying = true;
        m_subtitleGroup.DOFade(0, subtitleFadeDuration).OnComplete(delegate ()
        {
            // Set the state.
            m_subtitleShown = false;
            m_fadingSubtitleAnimationPlaying = false;
        });
    }

    void ShowCG(Sprite cg)
    {
        m_fadingCGAnimationPlaying = true;

        // Set cg before showing.
        m_cg.sprite = cg;

        // Set animator trigger.
        animCG.SetTrigger("Show");
    }

    public void OnShownCG()
    {
        // Set the state.
        m_cgShown = true;
        m_fadingCGAnimationPlaying = false;
        m_cgToFadeInOnShown = null;
    }

    Sprite m_cgToFadeInOnShown = null;
    void FadeIntoCG(Sprite cg)
    {
        m_cgToFadeInOnShown = cg;
        HideCG();

        // As long as cg m_cgToFadeInOnShown, 
        // OnHiddenCG() will handle it and show it after fading out.
    }

    void HideCG()
    {
        m_fadingCGAnimationPlaying = true;

        // Set animator trigger.
        animCG.SetTrigger("Hide");
    }

    public void OnHiddenCG()
    {
        // Set the state.
        m_cgShown = false;
        m_fadingCGAnimationPlaying = false;

        // If to fade into a cg after showing, do it.
        if (m_cgToFadeInOnShown != null)
            ShowCG(m_cgToFadeInOnShown);
    }
}
