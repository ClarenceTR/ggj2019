﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour
{
    TorchController m_manager;
    bool m_initialized;

    void Start()
    {
        StartCoroutine(C_Initialize());
    }

    IEnumerator C_Initialize()
    {
        yield return new WaitUntil(() => (m_manager = FindObjectOfType<TorchController>()) != null);
        yield return new WaitUntil(() => m_manager.player != null);
        yield return new WaitUntil(() => m_manager.coldControl != null);
        m_initialized = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!m_initialized) return;

        // Girl warmed by torch.
        if (collision.gameObject == m_manager.player.gameObject)
        {
            m_manager.coldControl.TorchWarmsGirl = true;
        }
        // Todo: sheep torch.
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!m_initialized) return;

        // Girl leaves torch.
        if (collision.gameObject == m_manager.player.gameObject)
        {
            m_manager.coldControl.TorchWarmsGirl = false;
        }
        // Todo: sheep torch.
    }
}
