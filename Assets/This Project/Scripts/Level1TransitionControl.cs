﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1TransitionControl : MonoBehaviour
{
    CanvasTransition canvasTransition;
    private void Start()
    {
        canvasTransition = FindObjectOfType<CanvasTransition>();
        StartCoroutine(C_WaitToFade(1f));
    }

    IEnumerator C_WaitToFade(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        canvasTransition.FadeInScene(2f);
    }
}
