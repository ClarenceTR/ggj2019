﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Luminosity.IO;

public class SheepController : Singleton<SheepController>
{
    [Header("Movement")]
    [Range(0, 100)]
    [Tooltip("The speed of the sheep.")]
    public float speed = 1f;
    [Tooltip("When the scale.x is 1, are the sheep sprites facing right? It determines the facing logic.")]
    public bool spriteFacingRight = false;
    [Range(0, 1)]
    [Tooltip("Within which distance the sheep will stop following the target.")]
    public float stopDistance = 0.1f;
    [Range(0.01f, 10)]
    [Tooltip("Above which speed the sheep will change facing direction.")]
    public float facingVelocityThreshold = 3f;

    [Header("Ground Check")]
    public Transform groundCheck;
    public float groundCheckRadius = 0.1f;
    public LayerMask groundLayer = 1 << 8;

    [Header("Target to follow")]
    [Tooltip("The transform of the target the sheep may follow.")]
    public Transform target;

    [Header("Collision Check")]
    [Range(0, 2)]
    [Tooltip("The radius of the collision detection. It overrides the radius of the collision trigger.")]
    public float collisionRadius = 0.1f;

    [Header("Hold")]
    [Tooltip("Objects under which layer can hold me.")]
    public LayerMask holdLayer = 1 << 9;
    [Tooltip("Objects within which circle can hold me. A circle is visualized in the scene window.")]
    public float holdRange = 1f;

    [Header("Swell")]
    [Tooltip("Gravity scale when floating up. It overrides the gravity scale in Rigidbody.")]
    public float revertGravity = -0.1f;
    [Tooltip("Gravity scale when normal. It overrides the gravity scale in Rigidbody.")]
    public float normalGravity = 1f;

    Rigidbody2D rb;
    Animator anim;
    bool m_facingRight = false;
    [SerializeField] bool m_autoFacing = true;
    bool m_isGrounded = true;
    [SerializeField] bool m_followingTarget;
    Vector2? m_targetPosition = null;
    [SerializeField] bool m_held = false;
    bool m_swollen = false;
    CharacterController2D m_holder;
    bool m_enableInput = true;
    Collider2D[] cols;
    CircleCollider2D m_collisionTrigger;
    IndicatorShower m_indicator;

    public bool FacingRight
    {
        get => m_facingRight;
        set
        {
            m_facingRight = value;
            Vector3 scaler = transform.localScale;
            scaler.x = Mathf.Abs(scaler.x) * (spriteFacingRight ^ m_facingRight ? -1 : 1);
            transform.localScale = scaler;
        }
    }

    public bool EnableInput { get => m_enableInput; set => m_enableInput = value; }

    public bool AutoFacing { get => m_autoFacing; set => m_autoFacing = value; }

    public bool FollowingTarget { get => m_followingTarget; set => m_followingTarget = value; }

    public bool IsKinematic
    {
        set
        {
            FollowingTarget = value ? false : FollowingTarget;
            rb.isKinematic = value;
            // Reset the velocity.
            rb.velocity = Vector2.zero;
            foreach (Collider2D col in cols)
                col.enabled = !value;
            AutoFacing = value ? false : true;
        }
    }

    public bool DuringAnimation
    {
        get => m_holdingAnimationPlaying | m_ReleasingAnimationPlaying | m_ShrinkingAnimationPlaying | m_SwellingAnimationPlaying;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = normalGravity;    // Overrides the gravity scale at the beginning.
        anim = GetComponent<Animator>();
        var allcols = GetComponents<Collider2D>();
        cols = allcols.Where((c) => c.isTrigger == false).ToArray();
        m_collisionTrigger = allcols.Where((c) => c.isTrigger).FirstOrDefault() as CircleCollider2D;
        m_collisionTrigger.radius = collisionRadius;    // Overrides the trigger radius at the beginning.
        m_indicator = FindObjectOfType<IndicatorShower>();
        //m_indicator.indicatorRadius = holdRange;
    }

    bool m_inputUseDown = false;
    bool m_inputSwellDown = false;
    private void Update()
    {
        m_inputUseDown = InputManager.GetButtonDown("Use");
        m_inputSwellDown = InputManager.GetButtonDown("Swell");
    }

    private void FixedUpdate()
    {
        m_isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);

        // Detect if it's in the hold range.
        var collision = Physics2D.OverlapCircle(transform.position, holdRange, holdLayer);
        if (m_isGrounded && !m_held && !m_swollen && collision)
        {
            // Show the indicator.
            m_indicator.ShowIndicator();
        }
        else
        {
            m_indicator.HideIndicator();
        }

        if (EnableInput)
        {
            if (m_inputUseDown)
            {
                if (!m_swollen)
                {
                    // If the sheep is being held.
                    if (m_held)
                    {
                        // Release the sheep.
                        if (!DuringAnimation) BeReleased();
                    }
                    else
                    {
                        if (collision)
                        {
                            // In range. 
                            // Hold the sheep.
                            if (!DuringAnimation) BeHeld(collision);
                        }
                        else
                        {
                            // Sheep comes to the target position.
                            FollowingTarget = true;
                            m_targetPosition = target.position;

                            // Show the sheep calling vfx.
                            IndicatorShower indicatorShower = target.GetComponent<IndicatorShower>();
                            indicatorShower?.ShowCallingVFX();
                        }
                    }
                }
            }

            if (m_inputSwellDown)
            {
                if (!m_held)
                {
                    if (!m_swollen)
                    {
                        if (!DuringAnimation) Swell();
                    }
                    else
                    {
                        if (!DuringAnimation) Shrink();
                    }
                }
            }
        }

        if (FollowingTarget)
        {
            if (m_targetPosition.HasValue)
                if (MoveTowards(m_targetPosition.Value))
                {
                    // Stops.
                    FollowingTarget = false;
                    m_targetPosition = null;
                }
        }

        // Set facing
        if (AutoFacing)
        {
            float v = rb.velocity.x;
            if (Mathf.Abs(v) > facingVelocityThreshold)
                FacingRight = v > 0;

            // Debug
            //ScreenMsgPrinter.Instance.Print("Speed: " + rb.velocity);
        }
    }

    public void Move(bool right)
    {
        // Move right.
        var v = new Vector2((right ? 1 : -1) * speed, rb.velocity.y);
        rb.velocity = v;
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
        //    rb.MovePosition(rb.position + (right ? Vector2.right : Vector2.left) * speed * Time.deltaTime);
    }

    public void StopMoving()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        anim.SetFloat("Speed", 0);
    }

    public bool MoveTowards(Vector2 position, bool considerCollision = true)
    {
        // This sheep can only move horizontally. Y value is ignored.
        //rb.MovePosition(Vector2.MoveTowards(rb.position, new Vector2(position.x, rb.position.y), speed * Time.deltaTime));
        float dist = rb.position.x - position.x;
        rb.velocity = new Vector2((dist < 0 ? 1:-1) * speed, rb.velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));

        // Returns if reaches the position.
        bool res = Mathf.Abs(rb.position.x - position.x) < (considerCollision ? stopDistance + collisionRadius : stopDistance);
        return res;
    }

    bool m_holdingAnimationPlaying = false;
    public void BeHeld(Collider2D holder)
    {
        m_holder = holder.GetComponent<CharacterController2D>();
        // If the holder is not on the ground, do nothing and quit. 
        if (!m_holder.IsGrounded) return;

        m_holdingAnimationPlaying = true;

        // Disable any input.
        EnableInput = false;
        m_holder.EnableInput = false;

        // Stop the holder's movement.
        m_holder.IsKinematic = true;

        // Face each other.
        float holderX = holder.transform.position.x;
        float meX = transform.position.x;
        // If holderX < meX, holder-> --- <-sheep.
        // Else, sheep-> --- <-holder.
        m_holder.FacingRight = holderX < meX;
        FacingRight = !(holderX < meX);

        // Set sheep as kinematic.
        IsKinematic = true;

        // Put the sheep onto the holder.
        m_holder.Grab(transform);

        // Play hold animation.
        var animHolder = m_holder.GetComponentInChildren<Animator>();
        animHolder.SetTrigger("Hold");

        // Set the holder's animation state as "Held".
        animHolder.SetBool("Held", true);

        // The animator will call OnHeld() once is done.

        // Set held as true.
        m_held = true;
    }

    public void OnHeld()
    {
        // Set sheep animation.
        anim.SetBool("Held", true);

        // Reenable inputs.
        EnableInput = true;
        m_holder.EnableInput = true;

        m_holder.IsKinematic = false;

        // Slow the holder's speed, and disable jump.
        Girl girl = m_holder.GetComponent<Girl>();
        if (girl) girl.State = GirlState.Holding;

        m_holdingAnimationPlaying = false;
    }

    bool m_ReleasingAnimationPlaying = false;
    public void BeReleased()
    {
        m_ReleasingAnimationPlaying = true;

        if (m_holder)
        {
            // Disable any input.
            EnableInput = false;
            m_holder.EnableInput = false;

            // Stop the holder's movement.
            m_holder.IsKinematic = true;

            // holder puts down the sheep.
            // Play release animation.
            var anim = m_holder.GetComponentInChildren<Animator>();
            anim.SetTrigger("Release");
        }
    }

    public void OnReleased()
    {
        // holder relase the sheep she's grabing.
        m_holder.HandsOff();

        // Set sheep animation.
        anim.SetBool("Held", false);

        // Set sheep holder's animation.
        var animHolder = m_holder.GetComponentInChildren<Animator>();
        animHolder.SetBool("Held", false);

        // Sheep turns away from the holder.
        // If holder->, sheep->.
        // Else <-sheep, <-holder.
        FacingRight = m_holder.FacingRight;

        // Enable holder's input.
        m_holder.EnableInput = true;

        m_holder.IsKinematic = false;

        // Resume holder's speed and jumpability.
        Girl girl = m_holder.GetComponent<Girl>();
        if (girl) girl.State = GirlState.Normal;

        // The trigger detectors will move the sheep until no collision, and set isKinematic back to false.

        // Exit the "Held" animation state.
        anim.SetBool("Held", false);

        // Set held as false.
        m_held = false;

        // Consider the releasing animaiton is done only after triggerExit.
        // So the flag will be set at the end of OnTriggerExit2D.

    }

    bool m_SwellingAnimationPlaying = false;
    public void Swell()
    {
        m_SwellingAnimationPlaying = true;

        // Disable the input on sheep.
        EnableInput = false;

        anim.SetTrigger("Swell");

        // After the swell animation is over, OnSwollen() will be called.
    }

    public void OnSwollen()
    {
        anim.SetBool("Swollen", true);
        m_swollen = true;

        // Reenable the input on sheep.
        EnableInput = true;

        // Sheep starts to float.
        rb.gravityScale = revertGravity;

        m_SwellingAnimationPlaying = false;
    }

    bool m_ShrinkingAnimationPlaying = false;
    public void Shrink()
    {
        m_ShrinkingAnimationPlaying = true;

        // Disable the input on sheep.
        EnableInput = false;

        anim.SetTrigger("Shrink");

        // After the shrink animation is over, OnShrunken() will be called.
    }

    public void OnShrunken()
    {
        anim.SetBool("Swollen", false);
        m_swollen = false;

        // Reenable the input on sheep.
        EnableInput = true;

        // Sheep starts to fall.
        rb.gravityScale = normalGravity;

        m_ShrinkingAnimationPlaying = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, collisionRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, holdRange);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        // As long as it's bumping with the holder's collider, keep moving.
        if (m_holder && collision.gameObject == m_holder.gameObject)
        {
            if (m_ReleasingAnimationPlaying && !m_held)
            {
                Move(FacingRight);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (m_holder && collision.gameObject == m_holder.gameObject)
        {
            if (m_ReleasingAnimationPlaying)
            {
                if (!m_held)
                {
                    // Turn isKinematic back off.
                    IsKinematic = false;

                    // Reenable sheep input.
                    EnableInput = true;

                    // Set m_holder as null.
                    m_holder = null;

                    // Set the animation as done.
                    m_ReleasingAnimationPlaying = false;
                }
            }
        }
    }
}
