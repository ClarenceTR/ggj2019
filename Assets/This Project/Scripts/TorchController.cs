﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchController : MonoBehaviour
{
    [HideInInspector] public Girl player;
    [HideInInspector] public ColdBarController coldControl;

    void Start()
    {
        player = FindObjectOfType<Girl>();
        coldControl = FindObjectOfType<ColdBarController>();
    }
}
